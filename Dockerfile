FROM python:3.10
#EXPOSE ${BACKEND_PORT}
RUN apt-get update
RUN apt-get install ffmpeg libsm6 libxext6  -y
RUN pip install cmake==3.22.4
RUN pip install --upgrade pip

WORKDIR /home
ENV TZ=Europe/Moscow

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt


COPY . .

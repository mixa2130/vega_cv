# vega_cv

Целью настоящей работы является разработка комплекса приложений, позволяющего сократить непроизводительные затраты времени преподавателей при проведении аудиторных занятий. 
Следующий комплекс приложений включает в себя:
- приложение для создания и постобработки фотографий маркерной доски, расположенной в учебной аудитории;
- приложение для отметки студентов по фотографиям из учебной аудитории.

## Project overview

![img.png](README_photos/diplom_main.png)

## Installation

### Конфигурация

Бот конфигурируется с помощью файла переменных окружений .env в рабочей директории.
Пример .env:

~~~dotenv
RABBITMQ_PORT=5672
RABBITMQ_MANAGEMENT_UI_PORT=15672
RABBITMQ_DEFAULT_USER=gree
RABBITMQ_DEFAULT_PASS=0000
MONGODB_PORT=27017
MONGO_INITDB_ROOT_USERNAME=gree
MONGO_INITDB_ROOT_PASSWORD=0000
BROKER_URI=amqp://gree:0000@rabbitmq:5672//
BACKEND_URI=mongodb://gree:0000@mongodb:27017
# RSTP camera connection uri
RTSP_IMAGE_URI=rtsp://192.0.0.0:5543/user=admin_password
# Куда примонтировать директорию с хоста в докер контейнере
RESULT_MOUNTED_DIR=/home/mount_dir
BACKEND_PORT=80
POSTGRES_CONNECTION_URI=postgres://gree:0000@host.docker.internal:5432/tests
# Local tests
#RTSP_IMAGE_URI=0
BACKEND_HOST=0.0.0.0
#POSTGRES_CONNECTION_URI="postgres://gree:8771@localhost/tests"
#BROKER_URI="amqp://gree:8771@127.0.0.1:5672//"
#BACKEND_URI="mongodb://gree:8771@127.0.0.1:27017"
#RESULT_MOUNTED_DIR="/home/gree/PycharmProjects/vega_cv"
~~~

### Docker

Docker-compose launch
~~~
docker-compose up -d --build
~~~



### Локальное тестировние

~~~bash
uvicorn server:app
celery -A celery_task_app.worker worker -l info -Q camera_rtsp -c 1 -n worker1.%h
celery -A celery_task_app.worker worker -l info -Q wbd_main -c 2 -n worker2.%h
celery -A celery_task_app.worker worker -l info -Q faceId -c 3 -n worker3.%h
~~~
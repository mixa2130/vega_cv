class FailedToGrabImage(Exception):
    def __repr__(self):
        return "Failed to grab frame from specified rtsp stream"


class FailedToExecuteTask(Exception):
    def __init__(self, exc: str):
        self.exc = exc

    def __repr__(self):
        return f"Failed to Execute task: {self.exc}"


class IncorrectImgPath(Exception):
    def __init__(self, path: str):
        self.path = path

    def __repr__(self):
        return f"There are no image at {self.path}. The img_path must be relative to the mounted directory!"

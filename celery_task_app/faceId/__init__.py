def face_recognition(groups: list, img_path: str, el_date: str, element_id: int, debug: bool = False):
    import os
    from .face_id import recognize_persons
    from .postgres_connector import get_embeddings, mark_students_in_widget
    from .faceid_logger import FaceIdLogger

    logger = FaceIdLogger()
    embeddings = get_embeddings(groups)

    students_ids = recognize_persons(img_path=img_path,
                                     known_persons=embeddings,
                                     debug_mode=debug,
                                     logger=logger)
    logger.info(f"Распознаны следующие студенты: {students_ids}")
    if students_ids:
        mark_students_in_widget(el_date=el_date, element_id=element_id, recognized_students_ids=students_ids)
    return students_ids


def add_student_biometric(student_id: int, img_path: str) -> bool:
    from .face_id import create_embeddings
    from .postgres_connector import add_embedding, check_student_by_id

    if check_student_by_id(student_id):
        # Student with such id exists
        student_embedding = create_embeddings(img_path)
        add_embedding(student_id=student_id, embedding=student_embedding)
        return True
    return False

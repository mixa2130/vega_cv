"""
Embedding - преобразование изображения лица в числовой вектор.
Нейронная сеть принимает на вход изображение, а на выходе возвращает числовой вектор,
характеризующий основные признаки данного лица.
Данный вектор и называется embedding-м.
"""
import os
from collections import Counter
import face_recognition
import cv2


def draw_faces(img, face_boxes: list, student_ids: list, filename: str):
    """
    :param img: opencv image
    :param face_boxes: faces coordinates
    :param filename: output filename
    :param student_ids: message near rectangle
    """
    from .postgres_connector import get_names
    _names: dict = get_names([el for el in set(student_ids) if el != -1])

    custom_names = list()
    for el in student_ids:
        if el != -1 and _names.get(str(el)) is not None:
            custom_names.append(_names.get(str(el)))
        else:
            custom_names.append('Unknown')

    for ((x, y, w, h), student) in zip(face_boxes, custom_names):
        print(student)
        cv2.rectangle(img, (h, x), (y, w), (0, 0, 255), 2)
        cv2.putText(img, student, (y, w), cv2.FONT_HERSHEY_COMPLEX,
                    3, (0, 255, 0), 4)

    cv2.imwrite(filename=filename, img=img)


def embedding_data(img):
    img_in_rgb = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    face_boxes = face_recognition.face_locations(img_in_rgb, model='hog')
    # if not face_boxes:
    #     face_boxes = face_recognition.face_locations(img_in_rgb, model='cnn')

    # вычисляем эмбеддинги для каждого лица
    embeddings = face_recognition.face_encodings(face_image=img_in_rgb,
                                                 known_face_locations=face_boxes,
                                                 num_jitters=20, model='large')
    return face_boxes, embeddings


def recognize_persons(known_persons: dict, img_path: str, logger, debug_mode: bool = False):
    """

    :param known_persons:
    :param img_path:
    :param debug_mode:
    :return:
    """
    logger.info("Распознаю студентов")
    names = list()

    original_img = cv2.imread(img_path)
    face_boxes, original_embeddings = embedding_data(original_img)

    for embedding in original_embeddings:
        # Compare original_embeddings with embeddings in known_persons["embeddings"]
        # Matches contain array with boolean values and True for the embeddings it matches closely
        # and False for rest
        matches = face_recognition.compare_faces(known_persons["embeddings"],
                                                 embedding, tolerance=0.5)

        if True in matches:
            # [('Ахтямова Гёля', 3), ('Ахтямова Гёля', 3), ('Лесин Борис', 1)]
            matchedIdxs = [known_persons["student_ids"][i]
                           for (i, b) in enumerate(matches) if b]
            # {('Ахтямова Гёля', 3): 2, ('Лесин Борис', 1): 1}
            counts = dict(Counter(matchedIdxs))
            names.append(max(counts, key=counts.get))
        else:
            names.append(-1)

    if debug_mode:
        logger.info("Выбран режим debug - распознанные студентьы будут отмечены на фотографии")
        from datetime import datetime

        debug_dir = os.path.join(os.getenv("RESULT_MOUNTED_DIR"), 'debug')
        if not os.path.exists(debug_dir):
            os.mkdir(debug_dir)
        filename = os.path.join(debug_dir, f'{datetime.now().strftime("%Y-%m-%d-%H-%M-%S")}.jpg')
        draw_faces(img=original_img, face_boxes=face_boxes, student_ids=names,
                   filename=filename)
        logger.info(f"Изображение с отмеченными студентами сохранено по пути: {filename}")
    return [el for el in set(names) if el != -1]


def create_embeddings(imagePath: str):
    original_image = cv2.imread(imagePath)
    _, encodings = embedding_data(original_image)
    return encodings[0]

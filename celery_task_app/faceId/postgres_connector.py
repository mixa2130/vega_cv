import os
from time import mktime
from datetime import datetime
import pickle

from typing import List, Generator
import numpy as np
from dotenv import load_dotenv
from contextlib import contextmanager

import psycopg2
from psycopg2 import Error, Binary
from psycopg2.extras import execute_values
from .faceid_logger import FaceIdLogger

load_dotenv()
LOGGER = FaceIdLogger()


class TransactionFailure(Exception):
    def __repr__(self):
        return "Failed to execute transaction"


@contextmanager
def db_session():
    connection = psycopg2.connect(os.getenv("POSTGRES_CONNECTION_URI"))
    connection.autocommit = False
    try:
        yield connection
    except (Exception, Error) as exc:
        LOGGER.error(f"Ошибка при выполнении транзакции: {repr(exc)}")
        connection.rollback()
        raise TransactionFailure
    finally:
        connection.close()


def check_student_by_id(student_id: int):
    with db_session() as session:
        with session.cursor() as cursor:
            cursor.execute(f"SELECT first_name FROM students WHERE id = {str(student_id)}")
            if cursor.fetchall():
                return True
            LOGGER.info(f"Отсутствуют студенты с id: {student_id}")
            return False


def add_embedding(student_id: int, embedding: np.array):
    with db_session() as session:
        with session.cursor() as cursor:
            bin_emb = pickle.dumps(embedding)
            cursor.execute("INSERT INTO faceid_embeddings(student_id, embedding) VALUES (%s, %s)", (
                student_id, Binary(bin_emb)))
            session.commit()
            LOGGER.info(f"Embedding для студента с id={student_id} успешно добавлен")


def get_embeddings(groups: List[int]):
    with db_session() as session:
        with session.cursor() as cursor:
            get_emb_sql = """SELECT fc.student_id, fc.embedding
                FROM faceid_embeddings fc
                JOIN students_to_groups stg on fc.student_id = stg.student_id
                WHERE stg.group_id IN %s"""
            cursor.execute(get_emb_sql, (tuple(groups),))
            rows = cursor.fetchall()
            return {"student_ids": [el[0] for el in rows],
                    "embeddings": [pickle.loads(row[1]) for row in rows]}


def mark_students_in_widget(el_date: str, element_id: int, recognized_students_ids: List[int]):
    with db_session() as session:
        dt = datetime.strptime(el_date, '%d-%m-%Y')
        unix_time = int(mktime(dt.timetuple()))
        with session.cursor() as cursor:
            ins_sql = """INSERT INTO a_lections(element_id, date) VALUES (%s, %s) RETURNING id"""
            cursor.execute(ins_sql, (element_id, unix_time))
            lection_id = cursor.fetchone()[0]
            LOGGER.info(f"Добавлена лекция с id={lection_id}")

            new_attendance_sql = """INSERT INTO s_attendance(element_id, student_id, lection_id, status) VALUES %s"""
            data = [(element_id, student_id, lection_id, '+') for student_id in recognized_students_ids]
            LOGGER.info(f"Отмечаемые студеньы: {data}")
            execute_values(cursor, new_attendance_sql, data)

            session.commit()
            LOGGER.info(f"Студенты успешно отмечены")


def get_names(student_ids: List[int]) -> dict:
    with db_session() as session:
        with session.cursor() as cursor:
            select_sts_sql = """SELECT id, first_name, middle_name FROM students WHERE id IN %s"""
            cursor.execute(select_sts_sql, (tuple(student_ids),))
            names = {}
            for el in cursor.fetchall():
                names[str(el[0])] = ' '.join((el[1], el[2]))
            return names

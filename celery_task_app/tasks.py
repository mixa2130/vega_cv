import os
from time import time, sleep
from datetime import datetime
from pathlib import Path

import cv2
from celery.states import FAILURE, STARTED, SUCCESS
from celery.utils.log import get_task_logger

from .worker import celery_app
from .wbd import wbd_postprocessing
from .faceId import face_recognition, add_student_biometric
from .exceptions import FailedToGrabImage, FailedToExecuteTask, IncorrectImgPath

celery_log = get_task_logger(__name__)
TMP_DIR = os.path.join('.', 'local')
Path(TMP_DIR).mkdir(parents=True, exist_ok=True)


@celery_app.task(bind=True, queue='camera_rtsp')
def create_rtsp_img(self, rtsp_uri):
    celery_log.info("Create rtsp image task")
    self.update_state(state=STARTED)

    res, rgb_img = cv2.VideoCapture(rtsp_uri).read()
    if res:
        img_path = os.path.join(os.getenv("RESULT_MOUNTED_DIR"), f'camera_rtsp_{str(int(time()))}.png')
        cv2.imwrite(img_path, rgb_img)
        self.update_state(state="WHITEBOARD IMAGE HAS BEEN CREATED")
        return img_path

    celery_log.error("Failed to grab frame from specified rtsp stream")
    self.update_state(state=FAILURE)
    raise FailedToGrabImage
    # return "/home/mount_dir/original.jpeg"
    # return "./celery_task_app/tmp/original.jpeg"


@celery_app.task(bind=True, queue='wbd_main')
def image_postprocessing(self, img_path: str, board: str, mode: str, prep: int, disc: int):
    celery_log.info('WBD detection task')
    self.update_state(state='POSTPROCESSING')

    output_path = os.path.join(os.getenv("RESULT_MOUNTED_DIR"),
                               datetime.now().strftime(f'%Y-%m-%d-%H-%M-%S_{disc}_{prep}') + ".pp%s.jpg")
    try:
        res = wbd_postprocessing(img_path, board, mode, output_path)
        if res:
            self.update_state(state=SUCCESS)
            return
    except Exception as exc:
        celery_log.error(f"Unable to postprocessing: {repr(exc)}")
        self.update_state(state=FAILURE)
        raise FailedToExecuteTask(repr(exc))
    else:
        self.update_state(state=FAILURE)
        raise FailedToExecuteTask("Unexpected behavior")
    finally:
        if os.path.exists(img_path):
            os.remove(img_path)


@celery_app.task(bind=True, queue="faceId")
def create_face_recognition_task(self, groups: list, img_path: str, debug_mode: bool, el_date: str, element_id: int):
    celery_log.info("FaceId task")
    self.update_state(state=STARTED)

    img_path = os.path.join(os.getenv("RESULT_MOUNTED_DIR"), img_path)
    if not os.path.exists(img_path):
        # There may be a delay on the docker mount
        celery_log.warning(f"There are no image at {img_path}. Sleep 5 sec and recheck")
        sleep(5)
        if not os.path.exists(img_path):
            celery_log.error(f"There are no image at {img_path}")
            self.update_state(state=FAILURE)
            raise IncorrectImgPath(img_path)

    try:
        res: list = face_recognition(groups=groups, img_path=img_path, debug=debug_mode,
                                     el_date=el_date, element_id=element_id)
        celery_log.info(f"Распознаны: {res}")
        self.update_state(state=SUCCESS)
        return res
    except Exception as exc:
        celery_log.error(repr(exc))
        self.update_state(state=FAILURE)
        return []
    finally:
        if os.path.exists(img_path):
            os.remove(img_path)


@celery_app.task(bind=True, queue="faceId")
def create_add_biometric_task(self, student_id: int, img_path: str):
    celery_log.info("Add biometric task")
    self.update_state(state=STARTED)

    img_path = os.path.join(os.getenv("RESULT_MOUNTED_DIR"), img_path)
    if not os.path.exists(img_path):
        # There may be a delay on the docker mount
        celery_log.warning(f"There are no image at {img_path}. Sleep 5 sec and recheck")
        sleep(5)
        if not os.path.exists(img_path):
            celery_log.error(f"There are no image at {img_path}")
            self.update_state(state=FAILURE)
            raise IncorrectImgPath(img_path)

    try:
        res = add_student_biometric(student_id, img_path)
        if res:
            self.update_state(state=SUCCESS)
            return
    except Exception as exc:
        celery_log.error(repr(exc))
        self.update_state(state=FAILURE)
        raise FailedToExecuteTask(repr(exc))
    else:
        self.update_state(state=FAILURE)
        raise FailedToExecuteTask(f"There are no student with id {student_id}")
    finally:
        if os.path.exists(img_path):
            os.remove(img_path)

import os
import json

from logging import Logger
from pathlib import Path

import cv2 as cv
import numpy as np

from .wbdlogger import WBDLogger

TMP_DIR = os.path.join('.', 'local')
Path(TMP_DIR).mkdir(parents=True, exist_ok=True)
ROOT_LOGGER: Logger = WBDLogger()


def wbd_postprocessing(img_path: str, board_name: str, wbd_mode: str, output_path: str) -> bool:
    """

    :param img_path: path to original image to be processed
    :param board_name: name of board
    :param wbd_mode: board side
    :param output_path: where to save image
    :return:
    """
    from .wbd import postprocessing, four_point_transform, \
        apply_brightness_contrast, unsharp_mask, undistort_img, UnsupportedBoardMode

    if not Path(img_path).exists():
        ROOT_LOGGER.error(f"Image {img_path} doesn't exist")
        return False

    original = cv.imread(img_path)
    weights_file_path = os.path.join(".", "celery_task_app", "wbd", "resources", "boards_weights.json")

    if not os.path.exists(weights_file_path):
        ROOT_LOGGER.error(f"There are no weights for {board_name}. Check {weights_file_path} existance")
        return False

    with open(weights_file_path) as f:
        data = json.load(f)[board_name][f"board_{wbd_mode}"]
        points = np.array(data["points"], dtype="float32")

        result = four_point_transform(image=original, pts=points,
                                      aspectRatio=data["aspectRatio"],
                                      mode=wbd_mode.lower(),
                                      logger=ROOT_LOGGER)

        result = unsharp_mask(image=result)
        result = apply_brightness_contrast(result, data["brightness"], data["contrast"])
        calibration_out_path = output_path % '1'

        # calibration + saving pp1
        undistort_img(image=result, mode=wbd_mode.lower(), output_path=calibration_out_path,
                      logger=ROOT_LOGGER, board_name=board_name)
        # postprocessing + saving pp2
        postprocessing(input_path=calibration_out_path, output_path=output_path % '2',
                       crop_weights=data["сrop_weights"], tmp_dir=TMP_DIR)

        return True

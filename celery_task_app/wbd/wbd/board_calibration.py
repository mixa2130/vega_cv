import os
import logging
from typing import NoReturn

import cv2
import numpy as np

WEIGHTS_PATH = os.path.join('.', 'celery_task_app', 'wbd', 'resources', 'calibration_weights.yml')


class UnsupportedCalibrationMode(Exception):
    def __init__(self, board, mode):
        self.mode = mode
        self.board = board

    def __repr__(self):
        return f"There are no weights for board '{self.board}' with mode '{self.mode}'"


def load_coefficients(mode: str, board_name: str):
    """Loads camera matrix and distortion coefficients."""
    cv_file = cv2.FileStorage(WEIGHTS_PATH, cv2.FILE_STORAGE_READ)

    # note we also have to specify the type to retrieve other wise we only get a
    # FileNode object back instead of a matrix
    camera_matrix = cv_file.getNode(f'K_{board_name}_board_{mode}').mat()
    dist_matrix = cv_file.getNode(f'D_{board_name}_board_{mode}').mat()

    cv_file.release()
    return camera_matrix, dist_matrix


def get_calibration_weights(mode: str, board) -> NoReturn:
    """Save the camera matrix and the distortion coefficients to given path/file."""

    _cm = None
    _dm = None

    if os.path.exists(WEIGHTS_PATH):
        _cm, _dm = load_coefficients(mode, board)
    return _cm, _dm


def undistort_img(image: np.ndarray, output_path: str, mode: str, logger: logging.Logger, board_name: str):
    """
    Undistort image using weights from yaml file.

    :param image: image for calibration
    :param output_path: where to save the file after processing
    :param mode: board side
    :param logger: logger
    :param board_name:
    """
    logger.info("Калибровка изображения")
    logger.info(f"Выбран режим калибровки: {mode}")

    mtx, dist = get_calibration_weights(mode=mode, board=board_name)
    if mtx is None or dist is None:
        raise UnsupportedCalibrationMode(board=board_name, mode=mode)

    h, w = image.shape[:2]
    newcameramtx, roi = cv2.getOptimalNewCameraMatrix(mtx, dist, (w, h), 1, (w, h))

    dst = cv2.undistort(image, mtx, dist, None, newcameramtx)
    # crop the image
    x, y, w, h = roi
    dst = dst[y:y + h, x:x + w]

    cv2.imwrite(output_path, dst)
    logger.info(f"Основной файл готов и записан по адресу: {output_path}")

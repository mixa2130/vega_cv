import os
from dotenv import load_dotenv
from celery import Celery

load_dotenv()

celery_app = Celery(
    'celery_app',
    broker=os.getenv('BROKER_URI'),
    backend=os.getenv('BACKEND_URI'),
    include=['celery_task_app.tasks']
)

from pydantic import BaseModel
from typing import List, Union


class WbdImage(BaseModel):
    board: str = 'main'
    mode: str = 'right'
    prep: int
    disc: int


class TaskAccepted(BaseModel):
    """ Celery task representation """
    task_id: str
    status: str = "Accepted"


class TaskRes(BaseModel):
    """ Celery task representation """
    task_id: str
    status: str
    result: Union[str, None] = None


class TaskFailed(BaseModel):
    task_id: str
    exception: str
    status: str = "FAILURE"


class FaceId(BaseModel):
    groups: List[int] = [22, 23]
    img_path: str = "in/DSC01716.png"
    date: str = '21-11-2000'
    element_id: int = 1157
    debug: bool = False


class AddBiometric(BaseModel):
    student_id: int
    img_path: str

import os
from dotenv import load_dotenv

from fastapi import FastAPI
from fastapi.responses import JSONResponse

from celery import chain
from celery.result import AsyncResult
from celery_task_app.tasks import image_postprocessing, create_rtsp_img,\
    create_face_recognition_task, create_add_biometric_task

from model import WbdImage, FaceId, TaskRes, TaskAccepted, TaskFailed, AddBiometric

load_dotenv()
app = FastAPI()
api_base_path = "/api/v1/board"


@app.post(f'{api_base_path}/wbd_img', status_code=201, response_model=TaskAccepted)
async def create_image(wbd_image: WbdImage):
    task_id = chain(create_rtsp_img.s(os.getenv('RTSP_IMAGE_URI')),
                    image_postprocessing.s(wbd_image.board, wbd_image.mode, wbd_image.prep,
                                           wbd_image.disc)).apply_async()

    return JSONResponse(status_code=201, content={'task_id': str(task_id), 'status': 'Accepted'})


@app.get(api_base_path + '/result/{task_id}', response_model=TaskRes, status_code=200,
         responses={202: {'model': TaskAccepted, 'description': 'Accepted: Not Ready'},
                    500: {'model': TaskFailed, 'description': "Failed to execute task"}})
async def board_result(task_id):
    """Fetch result for given task_id"""
    task = AsyncResult(task_id)
    if not task.ready():
        # print(app.url_path_for('board'))
        return JSONResponse(status_code=202, content={'task_id': str(task_id), 'status': str(task.state)})
    try:
        res = task.get()
        return JSONResponse(status_code=200,
                            content={'task_id': task_id, "result": res, 'status': 'SUCCESS'})
    except Exception as exc:
        return JSONResponse(status_code=500, content={'task_id': task_id, "exception": repr(exc), 'status': 'FAILURE'})


@app.post(f'{api_base_path}/faceid', status_code=201, response_model=TaskAccepted)
async def face_recognition(recognition_data: FaceId):
    task_id = create_face_recognition_task.delay(groups=recognition_data.groups,
                                                 img_path=recognition_data.img_path,
                                                 debug_mode=recognition_data.debug,
                                                 el_date=recognition_data.date,
                                                 element_id=recognition_data.element_id)

    return JSONResponse(status_code=201, content={'task_id': str(task_id), 'status': 'Accepted'})


@app.post(f'{api_base_path}/add_biometric', status_code=201, response_model=TaskAccepted)
async def add_student_biometric(student: AddBiometric):
    task_id = create_add_biometric_task.delay(student_id=student.student_id,
                                              img_path=student.img_path)
    return JSONResponse(status_code=201, content={'task_id': str(task_id), 'status': 'Accepted'})
